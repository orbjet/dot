(define-key key-translation-map [M-7] (kbd "|"))

(setq org-agenda-files '("/Volumes/gDC9EFcGRujx_0/Master-docs/"))

(setq org-todo-keywords
      '((sequence "TODO" "IN-PROGRESS" "WAITING" "|" "DONE" "CANCELED")))

(setq org-capture-templates
      '(("a" "My TODO task format." entry
         (file "/Volumes/gDC9EFcGRujx_0/Master-docs/todo.org")
         "* TODO %?
SCHEDULED: %t")))

(defun air-org-task-capture ()
  "Capture a task with my default template."
  (interactive)
  (org-capture nil "a"))

(define-key global-map (kbd "C-c c") 'air-org-task-capture)

(defun air-pop-to-org-agenda (split)
  "Visit the org agenda, in the current window or a SPLIT."
  (interactive "P")
  (org-agenda-list)
  (when (not split)
    (delete-other-windows)))

(define-key global-map (kbd "C-c C-c C-c") 'air-pop-to-org-agenda)

(setq org-agenda-custom-commands
      '(("d" "Daily agenda and all TODOs"
         ((tags "PRIORITY=\"A\""
                ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                 (org-agenda-overriding-header "High-priority unfinished tasks:")))
          (agenda "" ((org-agenda-ndays 1)))
          (alltodo ""
                   ((org-agenda-skip-function '(or

                                                   (org-agenda-skip-if nil '(scheduled deadline))))
                    (org-agenda-overriding-header "ALL normal priority tasks:"))))
         ((org-agenda-compact-blocks t)))))

(defun air-org-task-super-view ()
  "Capture a task with my default template."
  (interactive)
  (org-agenda nil "d"))


(define-key global-map (kbd "C-c v") 'air-org-task-super-view)
