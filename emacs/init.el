;; move point with S+arrows
(windmove-default-keybindings)

;; setup MELPA package repository
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

;; zenburn-emacs theme
(load-theme 'zenburn t)

;; enable ido mode
(ido-mode 1)
(setq ido-everywhere t)
(setq ido-enable-flex-matching t)

;; magit
(global-set-key (kbd "C-x g") 'magit-status)

;; company completition delay
(setq company-minimum-prefix-length 1
      company-idle-delay 0.0) ;; default is 0.2


;; set right alt key to none
(setq ns-right-alternate-modifier 'none)


